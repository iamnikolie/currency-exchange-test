CREATE TABLE IF NOT EXISTS `billing` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255));

CREATE TABLE IF NOT EXISTS `currency` (
    id INT AUTO_INCREMENT PRIMARY KEY,
	  billing_id INT REFERENCES billing(id)
    name VARCHAR(255));

CREATE TABLE IF NOT EXISTS `rate` (
    id INT AUTO_INCREMENT PRIMARY KEY,
	  from_currency_id INT REFERENCES currency(id),
	  to_currency_id INT REFERENCES currency(id),
    from_ammount INT,
    to_ammount INT);

INSERT INTO billing (name) VALUES ('Центробанк РФ');
INSERT INTO billing (name) VALUES ('Центробанк другой');
INSERT INTO billing (name) VALUES ('Центробанк УКР');
INSERT INTO billing (name) VALUES ('Вообще не банк');

INSERT INTO currency(billing_id, name) VALUES (1,'USD');
INSERT INTO currency(billing_id, name) VALUES (1,'UAH');
INSERT INTO currency(billing_id, name) VALUES (1,'RUR');
INSERT INTO currency(billing_id, name) VALUES (2,'USD');
INSERT INTO currency(billing_id, name) VALUES (2,'UAH');
INSERT INTO currency(billing_id, name) VALUES (3,'RUR');
INSERT INTO currency(billing_id, name) VALUES (3,'USD');
INSERT INTO currency(billing_id, name) VALUES (4,'UAH');
