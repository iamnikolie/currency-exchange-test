/**
 * Created by Support on 27.01.2016.
 */

var billingOne = $('#billing1');
var billingTwo = $('#billing2');
var flag = [false, false];
var added = false;
var results = [];

var currenciesPanel = $('#currencies-panel');
var ammountPanel = $('#ammount-panel');

var from = $('select[name="from_currency_id"]');
var to = $('select[name="to_currency_id"]');

$(document).ready(function(){

    billingOne.change(function(){
        if (billingOne.val() != '') flag[0] = true;
        else flag[0] = false;

        $.ajax({
            url: homeUrl+'/currency/billing/',
            type: 'get',
            dataType: 'json',
            data: {'id': billingOne.val()},
            result: results[0],
            success: function(data){
                results[0] = data;
                currencies();
            }
        });
    });

    billingTwo.change(function(){
        if (billingTwo.val() != '') flag[1] = true;
        else flag[1] = false;

        $.ajax({
            url:  homeUrl+'/currency/billing/',
            type: 'get',
            dataType: 'json',
            data: {'id': billingTwo.val()},
            success: function(data){
                results[1] = data;
                currencies();
            }
        });

    });

    $('select[name="from_currency_id"]').change(function(){
        console.log('1e');
        rates()
    });
    $('select[name="to_currency_id"]').change(function(){
        console.log('2r');
        rates()
    });


});

function currencies() {
    if (added == false && flag[0] && flag[1]){
        from.empty();
        to.empty();
        results.forEach(function(item, i){
            html = '';
            item.forEach(function(jitem, j) {
                html += '<option value="'+jitem.id+'">'+jitem.name+'</option>'
            });
            if (i == 0) {
                from.append(html);
                html = '';
            }
            if (i == 1) {
                to.append(html);
                html = '';
            }
        });
        currenciesPanel.fadeIn();
        ammountPanel.fadeIn();
        rates();
    }
}

function rates() {
    ammountPanel.empty();

    var from = $('select[name="from_currency_id"]');
    var to = $('select[name="to_currency_id"]');
    var data = {'from_currency_id': from.val(), 'to_currency_id': to.val()};
    $.ajax({
        url:  homeUrl+'/rate/get/',
        type: 'get',
        dataType: 'json',
        data: data,
        success: function(info) {
            console.log(info);
            if (info.status == "NeN"){
                var error_message = '<div class="panel panel-danger"><div class="panel-body">' +
                    'Информации о курсе не обнаружено' +
                    '</div></div>';
                ammountPanel.append(error_message);
            } else {
                content = '<div class="panel panel-danger"><div class="panel-body">' +
                    'Курс покупки - <b>' + info[0].from_ammount + '</b> <hr> Курс продажи <b>' +
                    info[0].to_ammount +
                    '</b></div></div>';
                ammountPanel.append(content);
            }
        }
    });
}
