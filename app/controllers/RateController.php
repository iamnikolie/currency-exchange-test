<?php

namespace app\controllers;
use yii\helpers\Url;

use Yii;
use yii\web\Controller;
use app\models\Currency;
use app\models\Billing;
use app\models\Rate;

class RateController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDelete($id=NULL)
    {
        if ($id === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            $this->goBack();
        }
        $item = Rate::findOne($id);
        if ($item === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            $this->goBack();
        }
        $item->delete();

        Yii::$app->session->setFlash('PostDeleted');
        $this->goBack();
    }

    public function actionGet($from_currency_id, $to_currency_id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $i = Rate::find()->where(['from_currency_id' => $from_currency_id, 'to_currency_id' => $to_currency_id])->all();
        if (empty($i)) return array('status' => 'NeN');
        else return $i;
    }

    public function actionCreate()
    {
        $model = new Rate;
        if ($_POST)
        {
            $model->from_currency_id = $_POST['from_currency_id'];
            $model->to_currency_id = $_POST['to_currency_id'];
            $model->from_ammount = $_POST['from_ammount'];
            $model->to_ammount = $_POST['to_ammount'];

            if ($model->save())
                $this->goBack();
        }

        $billing = new Billing;
        $currency = new Currency();

        echo $this->render('create', array(
            'model' => $model,
            'data' => [
                'currencies' => $currency->find()->all(),
                'billings' => $billing->find()->all()
            ]
        ));
    }
}