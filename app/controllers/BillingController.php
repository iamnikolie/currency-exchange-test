<?php

namespace app\controllers;
use yii\helpers\Url;

use Yii;
use yii\web\Controller;
use app\models\Billing;

class BillingController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDelete($id=NULL)
    {
        if ($id === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            Yii::$app->getResponse()->redirect(array(Url::home()));
        }
        $billing = Billing::findOne($id);
        if ($billing === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            return $this->goBack();
        }
        $billing->delete();
        Yii::$app->session->setFlash('PostDeleted');
        $this->goBack();

    }

    public function actionCreate()
    {
        $model = new Billing;
        if ($_POST)
        {
            $model->name = $_POST['name'];

            if ($model->save())
        	$this->goBack();
        }

        echo $this->render('create', array(
            'model' => $model
        ));
    }
}