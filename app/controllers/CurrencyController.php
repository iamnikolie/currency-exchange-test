<?php

namespace app\controllers;
use yii\helpers\Url;

use Yii;
use yii\web\Controller;
use app\models\Currency;
use app\models\Billing;

class CurrencyController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDelete($id=NULL)
    {
        if ($id === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            $this->goBack();
        }
        $item = Currency::findOne($id);
        if ($item === NULL)
        {
            Yii::$app->session->setFlash('PostDeletedError');
            $this->goBack();
        }
        $item->delete();

        Yii::$app->session->setFlash('PostDeleted');
        $this->goBack();
    }

    public function actionGet($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $m = new Currency();
        $i = $m->findOne($id);
        return $i;
    }

    public function actionBilling($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $m = new Currency();
        $i = $m->find()->where(['billing_id' => $id])->all();
        return $i;
    }

    public function actionCreate()
    {
        $model = new Currency;
        if ($_POST)
        {
            $model->name = $_POST['name'];
            $model->billing_id = $_POST['billing_id'];

            if ($model->save())
                $this->goBack();
        }

        $billing = new Billing;

        echo $this->render('create', array(
            'model' => $model,
            'data' => [
                'billings' => $billing->find()->all()
            ]
        ));
    }
}