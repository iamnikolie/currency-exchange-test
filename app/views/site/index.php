<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>

<script> var toWork = true; </script>

<div class="site-index">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <h1>Currency exchange - Test task</h1>
                <p>Lorem ipsum set dolor is..</p>
            </div>
            <hr>
            <div class="col-md-3">
                <div class="row">
                    <select name="billing1" id="billing1" class="form-control">
                        <option value="">
                            <?php foreach ($data['billings'] as $item): ?>
                                <option value="<?= Html::decode($item->id); ?>">
                                    <?= Html::decode($item->name); ?>
                                </option>
                            <?php endforeach; ?>
                    </select>
                </div>
                <hr>
                <div class="row">
                    <select name="billing2" id="billing2" class="form-control">
                        <option value="">
                            <?php foreach ($data['billings'] as $item): ?>
                        <option value="<?= Html::decode($item->id); ?>">
                            <?= Html::decode($item->name); ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <hr>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div id="currencies-panel" style="display: none;">
                    <div class="row">
                        <select name="from_currency_id" id="from_currency_id" class="form-control">
                        </select>
                    </div>
                    <hr>
                    <div class="row">
                        <select name="to_currency_id" id="to_currency_id" class="form-control">
                        </select>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div id="ammount-panel"></div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <p>Some text here</p>
    <hr>
	<?php if(!Yii::$app->user->isGuest): ?>
    <div class="row">
        <h2>Admin tools</h2>
        <hr>
        <div class="col-md-4">
            <?= Html::a('Add billing', array('billing/create'), array('class' => 'btn btn-primary pull-right')); ?>
            <div class="clearfix"></div>
            <h3>Billings</h3>
            <table class="table table-striped table-hover">
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td></td>
                </tr>
                <?php foreach ($data['billings'] as $item): ?>
                    <tr>
                        <td>
                            <?= Html::decode($item->id); ?>
                        </td>
                        <td><?= Html::decode($item->name); ?></td>
                        <td class="text-center">
                            <?= Html::a(NULL, array('billing/delete', 'id'=>$item->id), array('class'=>'fa fa-trash')); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="col-md-4">
            <?= Html::a('Add currency', array('currency/create'), array('class' => 'btn btn-primary pull-right')); ?>
            <div class="clearfix"></div>
            <h3>Curenncies</h3>
            <table class="table table-striped table-hover">
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Billing</td>
                    <td></td>
                </tr>
                <?php foreach ($data['currency'] as $item): ?>
                    <tr>
                        <td>
                            <?= Html::decode($item->id); ?>
                        </td>
                        <td><?= Html::decode($item->name); ?></td>
                        <td>
                            <?= Html::decode($item->billings->name); ?>
                        </td>
                        <td class="text-center">
                            <?= Html::a(NULL, array('currency/delete', 'id'=>$item->id), array('class'=>'fa fa-trash')); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="col-md-4">
            <?= Html::a('Add rate', array('rate/create'), array('class' => 'btn btn-primary pull-right')); ?>
            <div class="clearfix"></div>
            <h3>Rates</h3>
            <table class="table table-striped table-hover">
                <tr>
                    <td>#</td>
                    <td>From</td>
                    <td>To</td>
                    <td>Rates</td>
                    <td>Options</td>
                </tr>
                <?php foreach ($data['rate'] as $item): ?>
                    <tr>
                        <td>
                            <?= $item->id; ?>
                        </td>
                        <td>
                            #<?= $item->from_currency_id?> <?= $item->from->name; ?>
                        </td>
                        <td>
                            #<?= $item->to_currency_id?> <?= $item->to->name; ?>
                        </td>
                        <td>
                            <?= $item->from_ammount ?> / <?= $item->to_ammount; ?>
                        </td>
                        <td class="text-center">
                            <?= Html::a(NULL, array('rate/delete', 'id'=>$item->id), array('class'=>'fa fa-trash')); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
    <hr>
                <?php endif; ?>

</div>

