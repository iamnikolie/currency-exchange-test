
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>

    <form class="form-horizontal" method="POST">
        <div class="col-md-12">
            <h2>Add new rate</h2>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <select name="billing1" id="billing1" class="form-control">
                            <option value="">
                                <?php foreach ($data['billings'] as $item): ?>
                            <option value="<?= Html::decode($item->id); ?>">
                                <?= Html::decode($item->name); ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr>
                    <div class="row">
                        <select name="billing2" id="billing2" class="form-control">
                            <option value="">
                                <?php foreach ($data['billings'] as $item): ?>
                            <option value="<?= Html::decode($item->id); ?>">
                                <?= Html::decode($item->name); ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr>
                </div>
                <div class="col-md-3 col-md-offset-1">
                        <div id="currencies-panel" style="display: none;">
                            <div class="row">
                                <select name="from_currency_id" id="from_currency_id" class="form-control">
                                </select>
                            </div>
                            <hr>
                            <div class="row">
                                <select name="to_currency_id" id="to_currency_id" class="form-control">
                                </select>
                            </div>
                            <hr>
                        </div>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <div id="ammount-panel" style="display: none;">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="from_ammount" class="control-label">
                                    From
                                </label>
                            </div>
                            <div class="col-md-8">
                                <input placeholder="Amount.." type="text" class="form-control" name="from_ammount">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="to_ammount"  class="control-label">
                                    To
                                </label>
                            </div>
                            <div class="col-md-8">
                                <input  placeholder="Amount.."  type="text" class="form-control" name="to_ammount">
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 text-right">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <input type="submit" class="btn btn-success" value="Add">
                </div>
            </div>
        </div>

    </form>
