<?php use yii\helpers\Html; ?>


    <form class="form-horizontal" method="POST">

        <div class="form-group">
            <h2>Add new currency</h2>
            <hr>
            <div class="col-md-2">
                Name:
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="name">
            </div>
            <div class="col-md-3">
                <select name="billing_id" id="" class="form-control">
                    <?php foreach ($data['billings'] as $item): ?>
                        <option value="<?=$item->id?>"><?= $item->name?></option><?= $item->name?>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-1">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <input type="submit" class="btn btn-success" value="Add">
            </div>
        </div>

    </form>
