<?php

namespace app\models;
use app\models\Currency;

class Rate extends \yii\db\ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return 'rate';
    }

    /**
     * @return array primary key of the table
     **/
    public static function primaryKey()
    {
        return array('id');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'from_currency_id' => 'from_currency_id',
            'to_currency_id' => 'to_currency_id',
            'from_ammount' => 'Title',
            'to_ammount' => 'Billing ID'
        );
    }

    public function getFrom()
    {
        return $this->hasOne(Currency::className(), ['id' => 'from_currency_id'])
            ->from(Currency::tableName() . ' ta');
    }
    public function getTo()
    {
        return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
    }

}