<?php

namespace app\models;

class Currency extends \yii\db\ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @return array primary key of the table
     **/
    public static function primaryKey()
    {
        return array('id');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Title',
            'billing_id' => 'Billing ID'
        );
    }

    public function getBillings()
    {
        return $this->hasOne(Billing::className(), ['id' => 'billing_id']);
    }

}