<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Currency;

class Billing extends ActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * @return array primary key of the table
     **/
    public static function primaryKey()
    {
        return array('id');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name'
        );
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
        );
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function getCurrencies() {
        return $this->hasMany(Currency::className(), ['billing_id' => 'id']);
    }
}